FROM gitpod/workspace-full-vnc

RUN sudo apt-get update \
 && sudo apt-get install -y libgl1 libxkbcommon-x11-0 python3-pyqt5 \
 && sudo rm -rf /var/lib/apt/lists/*
 
